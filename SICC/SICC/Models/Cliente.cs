﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SICC.Models
{
    [Table("Clientes")]
    public class Cliente
    {
        [Key]
        public int ClienteId { get; set; }
        [Required]
        public string Nome { get; set; }
        [DisplayName("CPF")]
        [Required]
        public string Cpf { get; set; }
        [DisplayName("Número de contato 1")]
        public string NumeroContatoUm { get; set; }
        [DisplayName("Número de contato 2")]
        public string NumeroContatoDois  { get; set; }
        [DisplayName("Matrícula")]
        public string Matricula { get; set; }
        [DisplayName("Endereço")]
        public string Endereco { get; set; }
        [DisplayName("Data de Nascimento")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataNascimento { get; set; }
        [DisplayName("Data de Cadastro")]
        public DateTime DataCadastro { get; set; }
        [DisplayName("Convênio")]
        public int ConvenioId { get; set; }
        [ScaffoldColumn(false)]
        public bool ExclusaoLogica { get; set; }

        public Convenio ObjConvenio { get; set; }

        public ICollection<Observacao> ObservacaosCollection { get; set; }
        public ICollection<Emprestimo> EmprestimosCollection { get; set; }
    }
}
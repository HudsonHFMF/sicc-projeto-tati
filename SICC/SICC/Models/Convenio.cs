﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace SICC.Models
{
    [Table("Convenios")]
    public class Convenio
    {
        public int ConvenioId { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }

        [DisplayName("Observação")]
        public string Objservacao { get; set; }
        public bool Ativo { get; set; }

        public ICollection<Cliente> ClienteCollection { get; set; }
    }
}
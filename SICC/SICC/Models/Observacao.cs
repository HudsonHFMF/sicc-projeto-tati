﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SICC.Models
{
    [Table("Observacoes")]
    public class Observacao
    {
        [Key]
        public int ObservacaoId { get; set; }

        [DisplayName("Margem mensal")]
        public double? MargemMensal { get; set; }

        [Required]
        [DisplayName("Anotações")]
        public string Anotacoes { get; set; }

        [DisplayName("Data da observação")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataObservacao { get; set; }

        [DisplayName("Cliente")]
        public int ClienteId { get; set; }

        [ScaffoldColumn(false)]
        public bool ExclusaoLogica { get; set; }

        public Cliente ObjCliente { get; set; }
    }
}
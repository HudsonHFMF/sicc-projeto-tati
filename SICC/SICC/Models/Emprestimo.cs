﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SICC.Models
{
    [Table("Emprestimos")]
    public class Emprestimo
    {
        [Key]
        public int EmprestimoId { get; set; }

        [DisplayName("Valor do Emprestimo")]
        public decimal ValorEmprestimo { get; set; }

        [DisplayName("Conta")]
        public string Conta { get; set; }

        [DisplayName("Banco")]
        public string Banco { get; set; }

        [DisplayName("Quantidade de parcelas")]
        public int QuantidadeParcelas { get; set; }

        [DisplayName("Data do Emprestimo")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataEmprestimo { get; set; }

        [DisplayName("Cliente")]
        public int ClienteId { get; set; }

        [ScaffoldColumn(false)]
        public bool ExclusaoLogica { get; set; }

        public Cliente ObjCliente { get; set; }
    }
}
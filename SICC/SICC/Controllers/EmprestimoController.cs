﻿using System.Web.Mvc;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Controllers
{
    public class EmprestimoController : Controller
    {
        private readonly IEmprestimoRepository _emprestimoRepository;
        private readonly IClienteRepository _clienteRepository;

        public EmprestimoController(IEmprestimoRepository emprestimoRepository, IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
            _emprestimoRepository = emprestimoRepository;
        }
        
        // GET: Emprestimo
        public ActionResult Index(int id)
        {
            var cliente = _clienteRepository.GetById(id);
            TempData["Cliente"] = cliente;
            ViewBag.NomeCliente = cliente.Nome;
            ViewBag.ClienteId = cliente.ClienteId;
            return View(_emprestimoRepository.GetByCliente(id));
        }


        // GET: Emprestimo/Create
        public ActionResult Create()
        {
            ViewBag.Cliente = (Cliente)TempData["Cliente"];
            TempData.Keep("Cliente");
            return View();
        }

        // POST: Emprestimo/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Emprestimo objEmprestimo)
        {
            try
            {
                if (ValidateRequest && ModelState.IsValid)
                {
                    var ObjCliente = (Cliente) TempData["Cliente"];
                    objEmprestimo.ClienteId = ObjCliente.ClienteId;
                    _emprestimoRepository.Inserir(objEmprestimo);
                    ViewBag.NomeCliente = ObjCliente.Nome;
                    TempData.Keep("Cliente");
                    return View("Index", _emprestimoRepository.GetByCliente(objEmprestimo.ClienteId));
                }
                ViewBag.Cliente = (Cliente)TempData["Cliente"];
                TempData.Keep("Cliente");
                return View("Create", objEmprestimo);
            }
            catch
            {
                ViewBag.Cliente = (Cliente)TempData["Cliente"];
                TempData.Keep("Cliente");
                return View("Create", objEmprestimo);
            }
        }

        // GET: Emprestimo/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Cliente = (Cliente)TempData["Cliente"];
            TempData.Keep("Cliente");
            return View(_emprestimoRepository.GetById(id));
        }

        // POST: Emprestimo/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Emprestimo objEmprestimo)
        {
            try
            {
               _emprestimoRepository.Update(objEmprestimo);
                ViewBag.NomeCliente = ((Cliente) TempData["Cliente"]).Nome;
                TempData.Keep("Cliente");
                return View("Index", _emprestimoRepository.GetByCliente(objEmprestimo.ClienteId));
            }
            catch
            {
                return View(objEmprestimo);
            }
        }

        // GET: Emprestimo/Delete/5
        public ActionResult Delete(int id)
        {
            ViewBag.Cliente = (Cliente)TempData["Cliente"];
            TempData.Keep("Cliente");
            return View(_emprestimoRepository.GetById(id));
        }

        // POST: Emprestimo/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Emprestimo objEmprestimo)
        {
            try
            {
                objEmprestimo = _emprestimoRepository.GetById(objEmprestimo.EmprestimoId);
                objEmprestimo.ExclusaoLogica = true;
                _emprestimoRepository.Update(objEmprestimo);
                ViewBag.NomeCliente = ((Cliente)TempData["Cliente"]).Nome;
                TempData.Keep("Cliente");
                return View("Index", _emprestimoRepository.GetByCliente(objEmprestimo.ClienteId));
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System.Web.Mvc;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Controllers
{
    public class ObservacaoController : Controller
    {
        private readonly IObservacaoRepository _observacaoRepository;
        private readonly IClienteRepository _clienteRepository;

        public ObservacaoController(IObservacaoRepository observacaoRepository, IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
            _observacaoRepository = observacaoRepository;
        }
        // GET: Observacao
        public ActionResult Index(int id)
        {
            var cliente = _clienteRepository.GetById(id);
            TempData["Cliente"] = cliente;
            ViewBag.NomeCliente = cliente.Nome;
            ViewBag.ClienteId = cliente.ClienteId;
            return View(_observacaoRepository.GetByCliente(id));
        }

        // GET: Observacao/Create
        public ActionResult Create()
        {
            ViewBag.Cliente = (Cliente)TempData["Cliente"];
            TempData.Keep("Cliente");
            return View();
        }

        // POST: Observacao/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Observacao objObservacao)
        {
            try
            {
                if (ValidateRequest && ModelState.IsValid)
                {
                    var ObjCliente = (Cliente)TempData["Cliente"];
                    objObservacao.ClienteId = ObjCliente.ClienteId;
                    _observacaoRepository.Inserir(objObservacao);
                    ViewBag.NomeCliente = ObjCliente.Nome;
                    TempData.Keep("Cliente");
                    return View("Index", _observacaoRepository.GetByCliente(objObservacao.ClienteId));
                }
                ViewBag.Cliente = (Cliente)TempData["Cliente"];
                TempData.Keep("Cliente");
                return View("Create", objObservacao);
            }
            catch
            {
                return View("Create", objObservacao);
            }
        }

        // GET: Observacao/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Cliente = (Cliente)TempData["Cliente"];
            TempData.Keep("Cliente");
            return View(_observacaoRepository.GetById(id));
        }

        // POST: Observacao/Edit/5
        [HttpPost]
        public ActionResult Edit(Observacao objObservacao)
        {
            try
            {
                _observacaoRepository.Update(objObservacao);
                ViewBag.NomeCliente = ((Cliente)TempData["Cliente"]).Nome;
                TempData.Keep("Cliente");
                return View("Index", _observacaoRepository.GetByCliente(objObservacao.ClienteId));
            }
            catch
            {
                return View(objObservacao);
            }
        }

        // GET: Observacao/Delete/5
        public ActionResult Delete(int id)
        {
            ViewBag.Cliente = (Cliente)TempData["Cliente"];
            TempData.Keep("Cliente");
            return View(_observacaoRepository.GetById(id));
        }

        // POST: Observacao/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Observacao objObservacao)
        {
            try
            {
                objObservacao = _observacaoRepository.GetById(objObservacao.ObservacaoId);
                objObservacao.ExclusaoLogica = true;
                _observacaoRepository.Update(objObservacao);
                ViewBag.NomeCliente = ((Cliente)TempData["Cliente"]).Nome;
                TempData.Keep("Cliente");
                return View("Index", _observacaoRepository.GetByCliente(objObservacao.ClienteId));
            }
            catch
            {
                return View();
            }
        }
    }
}

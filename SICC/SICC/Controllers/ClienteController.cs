﻿using System;
using System.Web.Mvc;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IClienteRepository _clienteRepository;
        private readonly IConvenioRepository _convenioRepository;
        private readonly IEmprestimoRepository _emprestimoRepository;
        private readonly IObservacaoRepository _observacaoRepository;

        public ClienteController(IClienteRepository clienteRepository, IConvenioRepository convenioRepository, IEmprestimoRepository emprestimoRepository, IObservacaoRepository observacaoRepository)
        {
            _convenioRepository = convenioRepository;
            _clienteRepository = clienteRepository;
            _emprestimoRepository = emprestimoRepository;
            _observacaoRepository = observacaoRepository;
        }
        
        // GET: Cliente
        public ActionResult Index()
        {
            return View(_clienteRepository.GetAll());
        }

        // GET: Cliente/Details/5
        public ActionResult Details(int id)
        {
            var cliente = _clienteRepository.GetById(id);
            cliente.ObjConvenio = _convenioRepository.GetById(cliente.ConvenioId);
            ViewBag.QtdEmprestimos = _emprestimoRepository.GetByCliente(id).Count;
            ViewBag.QtdObservacoes = _observacaoRepository.GetByCliente(id).Count;
            return View(_clienteRepository.GetById(id));
        }

        // GET: Cliente/Create
        public ActionResult Create()
        {
            ViewBag.Convenios = new SelectList(_convenioRepository.GetAll(), "ConvenioId","Nome", "Selecione...");
            return View();
        }

        // POST: Cliente/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cliente objCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    objCliente.DataCadastro = DateTime.Now;
                    _clienteRepository.Inserir(objCliente);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Create", objCliente);
            }
        }

        // GET: Cliente/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Convenios = new SelectList(_convenioRepository.GetAll(), "ConvenioId", "Nome", "Selecione...");
            return View(_clienteRepository.GetById(id));
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cliente objCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _clienteRepository.Update(objCliente);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.Convenios = new SelectList(_convenioRepository.GetAll(), "ConvenioId", "Nome", "Selecione...");
                return View();
            }
        }

        // GET: Cliente/Delete/5
        public ActionResult Delete(int id)
        {
            return View(_clienteRepository.GetById(id));
        }

        // POST: Cliente/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Cliente objCliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    objCliente = _clienteRepository.GetById(objCliente.ClienteId);
                    objCliente.ExclusaoLogica = true;
                    _clienteRepository.Update(objCliente);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System.Web.Mvc;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Controllers
{
    public class ConvenioController : Controller
    {
        private readonly IConvenioRepository _convenioRepository;

        public ConvenioController(IConvenioRepository convenioRepository)
        {
            _convenioRepository = convenioRepository;
        }

        // GET: Convenio
        public ActionResult Index()
        {
            return View(_convenioRepository.GetAll());
        }

       // GET: Convenio/Create
        public ActionResult Novo()
        {
            return View();
        }

        // POST: Convenio/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Novo(Convenio objConvenio)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    objConvenio.Ativo = true;
                    _convenioRepository.Inserir(objConvenio);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Novo", objConvenio);
            }
        }

        // GET: Convenio/Edit/5
        public ActionResult Editar(int id)
        {
            return View(_convenioRepository.GetById(id));
        }

        // POST: Convenio/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(Convenio objConvenio)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _convenioRepository.Update(objConvenio);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Editar", objConvenio);
            }
        }

        // GET: Convenio/Delete/5
        public ActionResult Excluir(int id)
        {
            return View(_convenioRepository.GetById(id));
        }

        // POST: Convenio/Delete/5
        [HttpPost][ValidateAntiForgeryToken]
        public ActionResult Excluir(Convenio objConvenio)
        {
            try
            {
                if (ValidateRequest)
                {
                    objConvenio = _convenioRepository.GetById(objConvenio.ConvenioId);
                    objConvenio.Ativo = false;
                    _convenioRepository.Update(objConvenio);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

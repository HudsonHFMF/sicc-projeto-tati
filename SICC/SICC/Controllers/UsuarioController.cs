﻿using System;
using System.Web.Mvc;
using SICC.Models;
using SICC.Models.Seguranca;
using SICC.Repositorio.Interface;

namespace SICC.Controllers
{
    //[Autenticacao]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioController(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        // GET: Usuario
        public ActionResult Index()
        {
            return View(_usuarioRepository.GetByAtivos());
        }
        
        // GET: Usuario/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuario/Create
        [HttpPost]
        public ActionResult Create(Usuario objUsuario)
        {
            try
            {
                if (ValidateRequest)
                {
                    objUsuario.DataCadastro = DateTime.Now;
                    objUsuario.Ativo = true;
                    objUsuario.SenhaInspirada = true;
                    CriarMD5 encriptar = new CriarMD5();
                    objUsuario.Senha = encriptar.RetornarMD5(objUsuario.Login + "Primeira_Senh@123"); 

                    _usuarioRepository.Inserir(objUsuario);
                    return RedirectToAction("Index");
                }
                return View(objUsuario);
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuario/Edit/5
        public ActionResult Edit(int id)
        {
            return View(_usuarioRepository.GetById(id));
        }

        // POST: Usuario/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Usuario objUsuario)
        {
            try
            {
                if (ValidateRequest)
                {
                    var usuario = _usuarioRepository.GetById(objUsuario.UsuarioId);
                    usuario.Nome = objUsuario.Nome;
                    usuario.Login = objUsuario.Login;
                    usuario.Perfil = objUsuario.Perfil;
                    _usuarioRepository.Update(usuario);
                    return RedirectToAction("Index");
                }
                return View(objUsuario);
            }
            catch
            {
                return View();
            }
        }

        // GET: Usuario/Delete/5
        public ActionResult Delete(int id)
        {
            return View(_usuarioRepository.GetById(id));
        }

        // POST: Usuario/Delete/5
        [HttpPost]
        public ActionResult Delete(Usuario objUsuario)
        {
            try
            {
                if (ValidateRequest)
                {
                    objUsuario = _usuarioRepository.GetById(objUsuario.UsuarioId);
                    objUsuario.Ativo = false;
                    _usuarioRepository.Update(objUsuario);

                    return RedirectToAction("Index");
                }
                return View(objUsuario);
            }
            catch
            {
                return View(objUsuario);
            }
        }
        [HttpPost]
        public void ResetarSenha(int usuarioId)
        {
            var usuario = _usuarioRepository.GetById(usuarioId);
            usuario.SenhaInspirada = true;
            CriarMD5 encriptar = new CriarMD5();
            usuario.Senha = encriptar.RetornarMD5(usuario.Login + "Primeira_Senh@123");
            _usuarioRepository.Update(usuario);
        }
    }
}

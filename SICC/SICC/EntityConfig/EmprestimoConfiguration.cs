﻿using System.Data.Entity.ModelConfiguration;
using SICC.Models;

namespace SICC.EntityConfig
{
    public class EmprestimoConfiguration: EntityTypeConfiguration<Emprestimo>
    {
        public EmprestimoConfiguration()
        {
            HasKey(t => t.EmprestimoId);

            Property(t => t.ValorEmprestimo).IsRequired();
            Property(t => t.QuantidadeParcelas).IsRequired();
            Property(t => t.DataEmprestimo);

            HasRequired(t => t.ObjCliente).WithMany(t => t.EmprestimosCollection).HasForeignKey(t => t.ClienteId);
        }
    }
}
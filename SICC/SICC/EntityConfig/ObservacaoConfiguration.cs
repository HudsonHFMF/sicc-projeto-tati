﻿using System.Data.Entity.ModelConfiguration;
using SICC.Models;

namespace SICC.EntityConfig
{
    public class ObservacaoConfiguration : EntityTypeConfiguration<Observacao>
    {
        public ObservacaoConfiguration()
        {
            HasKey(t => t.ObservacaoId);

            Property(t => t.MargemMensal);
            Property(t => t.Anotacoes);
            Property(t => t.DataObservacao).IsRequired();

            HasRequired(t => t.ObjCliente).WithMany(t => t.ObservacaosCollection).HasForeignKey(t => t.ClienteId);
        }
    }
}
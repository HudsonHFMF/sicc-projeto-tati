﻿using System.Data.Entity.ModelConfiguration;
using SICC.Models;

namespace SICC.EntityConfig
{
    public class ConvenioConfiguration : EntityTypeConfiguration<Convenio>
    {
        public ConvenioConfiguration()
        {
            HasKey(t => t.ConvenioId);

            Property(t => t.Nome).IsRequired();
            Property(t => t.Sigla).IsRequired();
            Property(t => t.Objservacao).HasMaxLength(300);
            Property(t => t.Ativo).IsRequired();
        } 
    }
}
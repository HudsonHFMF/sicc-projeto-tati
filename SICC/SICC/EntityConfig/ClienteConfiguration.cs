﻿using System.Data.Entity.ModelConfiguration;
using SICC.Models;

namespace SICC.EntityConfig
{
    public class ClienteConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            HasKey(t => t.ClienteId);

            Property(t => t.Nome).IsRequired().HasMaxLength(200);
            Property(t => t.Cpf).IsRequired().HasMaxLength(11);
            Property(t => t.Matricula).IsRequired().HasMaxLength(20);
            Property(t => t.Endereco).HasMaxLength(400);
            Property(t => t.DataNascimento);
            Property(t => t.DataCadastro);
            Property(t => t.ExclusaoLogica).IsRequired();

            HasRequired(t => t.ObjConvenio).WithMany(t => t.ClienteCollection).HasForeignKey(t => t.ConvenioId);
        }
    }
}
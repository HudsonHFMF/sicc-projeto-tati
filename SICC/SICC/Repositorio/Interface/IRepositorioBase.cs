﻿namespace SICC.Repositorio.Interface
{
    public interface IRepositorioBase<TEntity> where TEntity : class
    {
        void Inserir(TEntity obj);
        TEntity GetById(int id);
        void Update(TEntity obj);
    }
}
﻿using System.Collections.Generic;
using SICC.Models;

namespace SICC.Repositorio.Interface
{
    public interface IEmprestimoRepository : IRepositorioBase<Emprestimo>
    {
        ICollection<Emprestimo> GetByCliente(int idCliente);
    }
}
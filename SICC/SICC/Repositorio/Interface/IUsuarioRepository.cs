﻿using System.Collections.Generic;
using SICC.Models;

namespace SICC.Repositorio.Interface
{
    public interface IUsuarioRepository : IRepositorioBase<Usuario>
    {
        Usuario GetByLogin(string login);
        ICollection<Usuario> GetByAtivos();
    }
}
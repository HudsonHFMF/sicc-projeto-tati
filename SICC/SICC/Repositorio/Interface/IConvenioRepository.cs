﻿using System.Collections.Generic;
using SICC.Models;

namespace SICC.Repositorio.Interface
{
    public interface IConvenioRepository : IRepositorioBase<Convenio>
    {
        ICollection<Convenio> GetAll();
    }
}
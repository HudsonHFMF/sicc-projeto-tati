﻿using System.Collections.Generic;
using SICC.Models;

namespace SICC.Repositorio.Interface
{
    public interface IObservacaoRepository : IRepositorioBase<Observacao>
    {
        ICollection<Observacao> GetByCliente(int idCliente);
    }
}
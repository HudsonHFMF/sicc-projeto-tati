﻿using System.Collections.Generic;
using SICC.Models;

namespace SICC.Repositorio.Interface
{
    public interface IClienteRepository : IRepositorioBase<Cliente>
    {
        ICollection<Cliente> GetAll();
    }
}
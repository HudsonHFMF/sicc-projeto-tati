﻿using System.Data.Entity;
using SICC.Repositorio.Interface;

namespace SICC.Repositorio
{
    public class RebositorioBase<TEntity> : IRepositorioBase<TEntity> where TEntity : class
    {
        protected Context DbContext = new Context();

        public void Inserir(TEntity obj)
        {
            DbContext.Set<TEntity>().Add(obj);
            DbContext.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public void Update(TEntity obj)
        {
            DbContext.Entry(obj).State = EntityState.Modified;
            DbContext.SaveChanges();
        }
        
    }
}
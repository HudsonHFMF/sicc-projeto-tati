﻿using System.Collections.Generic;
using System.Linq;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Repositorio
{
    public class UsuarioRepository: RebositorioBase<Usuario>, IUsuarioRepository
    {
        public Usuario GetByLogin(string login)
        {
           return DbContext.Usuarios.Single(x => x.Login == login);
        }

        public ICollection<Usuario> GetByAtivos()
        {
            return DbContext.Usuarios.Where(x => x.Ativo == true).ToList();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Repositorio
{
    public class ClienteRepository : RebositorioBase<Cliente>, IClienteRepository
    {
        public ICollection<Cliente> GetAll()
        {
            return DbContext.Set<Cliente>().Where(x => x.ExclusaoLogica == false).ToList();
            //return ? (IEnumerable<Militar>)Db.Set<Militar>().AsNoTracking()
            //    : Db.Set<Militar>().ToList();
        }
    }
}
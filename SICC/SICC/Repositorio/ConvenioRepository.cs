﻿using System.Collections.Generic;
using System.Linq;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Repositorio
{
    public class ConvenioRepository : RebositorioBase<Convenio>, IConvenioRepository
    {
        public ICollection<Convenio> GetAll()
        {
            return DbContext.Set<Convenio>().Where(x => x.Ativo == true).ToList();
            //return ? (IEnumerable<Militar>)Db.Set<Militar>().AsNoTracking()
            //    : Db.Set<Militar>().ToList();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Repositorio
{
    public class EmprestimoRepository : RebositorioBase<Emprestimo>, IEmprestimoRepository
    {
        public ICollection<Emprestimo> GetByCliente(int idCliente)
        {
            return DbContext.Emprestimos.Where(x => x.ClienteId == idCliente && x.ExclusaoLogica == false).ToList();
        }
    }
}
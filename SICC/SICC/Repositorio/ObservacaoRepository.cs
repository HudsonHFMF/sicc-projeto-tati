﻿using System.Collections.Generic;
using System.Linq;
using SICC.Models;
using SICC.Repositorio.Interface;

namespace SICC.Repositorio
{
    public class ObservacaoRepository : RebositorioBase<Observacao>, IObservacaoRepository
    {
        public ICollection<Observacao> GetByCliente(int idCliente)
        {
            return DbContext.Observacaos.Where(x => x.ClienteId == idCliente && x.ExclusaoLogica == false).ToList();
        }
    }
}